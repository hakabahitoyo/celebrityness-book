# Appendix B: An experiment of quantitative evaluation of the pro-celebrityness of the user discovery methods for Mastodon

In this book, the quantitative evaluation of the pro-celebrityness of the user discovery methods, also is called pro-celebrityness score, is defined as: the geometric mean of the numbers of followers of the users who the user discovery method recommends. In this chapter, the user discovery methods for Mastodon and other decentralized social networks are measured their pro-celebrityness score.

In the table below, the user discovery methods and their pro-celebrityness scores are shown. The labels A to K correspond with the [Chapter 2](02.md) in this book.

In this experiment, the user discovery methods were observed at 20:00 to 21:00 on 13th August 2018 in the Tokyo timezone. For the observation of the personalized user discovery methods, the vaginaplant<span>@</span>3.distsn.org [2] account was used. There is an exception that the hakabahitoyo<span>@</span>pawoo.net [3] was used for the observation of the Pawoo's _Who to follow_ panel. For the search engine, the keyword was “ハッピーシュガーライフ” [4] in this experiment. The numbers of followers were observed at 21:00 to 23:00 on the same day.

The pro-celebrityness score is the geometric mean of the numbers of followes of the users who the user discovery method recommends. For the user discovery methods which recommend more than 20 users, the first 20 users are computed. For the ones which do not recommend more than 20 users, the whole users are computed. The users who have no follower are treated as to have just 1 follower, because the 0 destructs the geometric mean.

<table>
<tr>
    <th>Label</th>
    <th>User discovery method</th>
    <th>Website</th>
    <th>Pro-celebrityness score</th>
</tr>
<tr>
    <td>A</td>
    <td>Mastodon Ranking (number of followers)</td>
    <td>[mastodon.userlocal.jp](http://mastodon.userlocal.jp/) (Outdated)</td>
    <td>1941.5</td>
<tr>
<tr>
    <td>B</td>
    <td>Recommended Followers</td>
    <td>[followlink.osa-p.net/recommend.html](https://followlink.osa-p.net/recommend.html) (Outdated)</td>
    <td>1312.7</td>
<tr>
<tr>
    <td>C</td>
    <td>Mastodon Famous Accounts</td>
    <td>[takulog.info/mastodon-famous-accounts](https://takulog.info/mastodon-famous-accounts/)</td>
    <td>1178.3</td>
<tr>
<tr>
    <td>D</td>
    <td>Mastodon Ranking (number of boosts)</td>
    <td>[mastodon.userlocal.jp/?fqdn=boost](http://mastodon.userlocal.jp/?fqdn=boost) (Outdated)</td>
    <td>1123.7</td>
<tr>
<tr>
    <td>E</td>
    <td>Pawoo</td>
    <td>[pawoo.net](https://pawoo.net/)</td>
    <td>558.3</td>
<tr>
<tr>
    <td>F</td>
    <td>Home timeline</td>
    <td>Embedded</td>
    <td>398.3</td>
<tr>
<tr>
    <td>G</td>
    <td>Federated Timeline</td>
    <td>Embedded</td>
    <td>404.9</td>
<tr>
<tr>
    <td>H</td>
    <td>Local timeline</td>
    <td>Embedded</td>
    <td>79.8</td>
<tr>
<tr>
    <td>I</td>
    <td>Tootsearch</td>
    <td>[tootsearch.chotto.moe](https://tootsearch.chotto.moe/)</td>
    <td>134.1</td>
<tr>
<tr>
    <td>J</td>
    <td>User Matching for Fediverse</td>
    <td>[distsn.org/user-match.html](https://distsn.org/user-match.html) (Broken link)</td>
    <td>39.6</td>
<tr>
<tr>
    <td>K</td>
    <td>Newcomers in Fediverse</td>
    <td>[distsn.org/user-new.html](https://distsn.org/user-new.html) (Broken link)</td>
    <td>1.8</td>
<tr>
</table>

## References

[1] Hakaba Hitoyo, ユーザーディスカバリーメソッドのフェアネスの定量的評価手法の提案,\
https://gitlab.com/distsn-documents/fairness-quantity (Broken link)


[2] https://3.distsn.org/vaginaplant (Broken link)


[3] https://pawoo.net/@hakabahitoyo


[4] 鍵空とみやき, ハッピーシュガーライフ,\
https://happysugarlife.tv (Broken link)

