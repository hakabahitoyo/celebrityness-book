# Pro- and Anti-Celebrityness of the User Recommendation Systems

This book is the updated version of https://distsn.gitbook.io/recommendation-fairness and https://gitlab.com/hakabahitoyo/recommendation-fairness-book. A Japanese translation of this book is available at https://gitlab.com/hakabahitoyo/celebrityness-book-ja.

## Abstract

In the internet, especially in the user-generated content platforms, the presence of users is oligopolized by few celebrities. I propose the term _internet of celebrities._ On the other hand, Mastodon and other federated social networks share the philosophy of _decentralization._ Usually they imagine that some federated instances share the power of their social network, actually _the decentralization of users_ is also motivated. The decentralization of users is the counterpart of the internet of celebrities.

A list of users which is ordered by the number of followers is a typical example of the design which supports the oligarchy of the users. Although the users who have many followers are likely to be attractive, it is unfair that the users who are not famous are rarely featured.

I also propose the term _pro-celebrityness_ in this book which means to support the oligarchy of users. By contrast, _anti-celebrityness_ means to support the decentralization of users. In this book, many websites are evaluated by their pro- and anti-celebrityness, especially the user recommendation systems for Mastodon.

## Table of contents

* [Chapter 1: The decentralization of users against the internet of celebrities](01.md)
* [Chapter 2: Qualitative and quantitative evaluation of the pro-celebrityness of user discovery methods](02.md)
* [Chapter 3: Designing an anti-celebrity user recommendation system](03.md)
* [Chapter 4: The decentralization of users for the internet of subsistence](04.md)
* [Chapter 5: Conclusion](05.md)
* [Chapter 6: Comments for updating](06.md)
* [Appendix A: A case study of qualitative evaluation of the pro-celebrityness of the user discovery methods for Mastodon](a.md)
* [Appendix B: An experiment of quantitative evaluation of the pro-celebrityness of the user discovery methods for Mastodon](b.md)
