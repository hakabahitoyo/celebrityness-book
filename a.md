# Appendix A: A case study of qualitative evaluation of the pro-celebrityness of the user discovery methods for Mastodon

## User recommendation systems for Mastodon

* Lists of recommended users
    * User Matching for Fedierse (Vaginaplant's)
    * Mastodon Recommended User Search (Cucmberium's)
        * Recommended users
        * Similar users
    * Recommended Followers (Osa's)
    * Potential friendship (Mastodon's official/embedded)
* _Who to follow_ panels
    * Pawoo
    * Pleroma
    * Halcyon
    * Misskey

## Other user discovery methods

* Lists of users
    * By their number of followers
    * By their number of boosts
    * Newcomers
    * Recently posted
    * Manual
    * Public profile endorsements (Mastodon's official/embedded)
* Timelines
    * Home
    * Federated
    * Local
* Search engines

## Summary of pro- and anti-celebrityness

* ↑ Anti-celebrity
* Newcomers
* User Matching for Fediverse (Vaginaplant's)
* Similar users (Cucmberium's)
* Search engines
* Potential friendship (Mastodon's official/embedded)
* Local timeline
* Federated timeline
* Home timeline
* Recently posted
* Pawoo's _who to follow_ panel
* Misskey's _who to follow_ panel
* List of users by their number of boosts
* Public profile endorsements (Mastodon's official/embedded)
* Manual guidebooks
* Recommended users (Cucmberium's)
* Recommended Followers (Osa's)
* List of users by their number of followers
* ↓ Pro-celebrity

## Case 1: List of users by their number of followers

### Implementation

* Mastodon Ranking [4] by User Local, Inc. 

### Specification

* Ordered by the number of followers.

### Pro-celebrityness

* Oldies effect: Once we had followed an user, we rarely unfollow the one, even if the user becomes not attractive anymore.
* Positive feedback: If we follow the users in this kind of list, it directly accelerates itself.
* Shrink-wrap effect: The list of users shows screen names, bios, and avatars. No content is shown apparently. We are attracted by the famous names and aesthetic avatars.

### Anti-celebrityness

* No power user effect: By definition.

### Summary

* One of the most pro-celebrity user discovery methods ever.

## Case 2: Recommended Followers

### Implementation

* Recommended Followers [5] by Osa

### Specification

* When you use this recommendation engine, it shows the users who are followed by the users who you follow.

### Pro-celebrityness

* The same oldies effect,  positive feedback, and shrink-wrap effect as the list of users by their number of followers.
* Recommends the celebrities in the small society.

### Anti-celebrityness

* No power user effect: By definition.

### Summary

* One of the most pro-celebrity user discovery methods ever.

## Case 3: Recommended users in Mastodon Recommended User Search

### Implementation

* Mastodon Recommended User Search [6] by Cucmberium

### Specification

* The users who your similar users follow.

### Pro-celebrityness

* The same oldies effect,  positive feedback, and shrink-wrap effect as the list of users by their number of followers.

### Anti-celebrityness

* No power user effect: By definition.

### Summary

* One of the most pro-celebrity user discovery methods ever.

## Case 4: Manual _guidebooks_

### Implementation

* Mastodon Famous Accounts [7] by Takumi

### Specification

* A blog post which is manually written.

### Pro-celebrityness

* Oldies effect: Manual guidebooks are rarely updated.
* Shrink-wrap effect: The authors of the guidebooks are attracted by famous names or aesthetic avatars.
* The authors of the guidebooks sometimes just follow some existing authority.

### Anti-celebrityness

* No positive feedback: Our action does not affect to the static documents.
* No power user effect: By definition.

### Summary

* Super pro-celebrityness user discovery.
* Anti-celebrity minded authors of the guidebooks are expected.

## Case 5: Public profile endorsements

### Implementation

* Mastodon's officiel/embedded [8]

### Specification

* The user pins other users on one's profile.

### Pro- and anti-celebrityness

* Almost same as the manual _guidebooks._

### Summary

* Super pro-celebrity user discovery.
* Endorsement is worship or flattery [9].

## Case 6: List of users by their number of boosts

### Implementation

* Mastodon Ranking [4] by User Local, Inc. 

### Specification

* Ordered by the number of boosts.

### Pro-celebrityness

* Oldies effect: The number of boosts is accumulated ever.
* Positive feedback: If we follow the user who is boosted well, we boost such kind of user's posts more often.
* Power user effect: More posts, more boosts.

### Anti-celebrityness

* No shrink-wrap effect: We read the post before we boost it.

### Summary

* Super pro-celebrity user discovery.
* Better than the user discovery methods which depend on the number of followers.

## Case 7: Misskey's _who to follow_ panel

### Implementation

* Misskey [10]

### Specification

* Posting in the last 7 days.
* Local users.
* Many followers.

### Pro-celebrityness

* The same positive feedback and shrink-wrap effect as the list of users by their number of followers.

### Anti-celebrityness

* Less oldies effect: Requires posting in the last 7 days.
* No power user effect: By definition.

### Summary

* Remarkably pro-celebrity user discovery.
* Better than the oldies effected user discovery methods.

## Case 8: Pawoo's _who to follow_ panel

### Implementation

* Pawoo [11, 12]

### Specification

* Following in pixiv.
* Popular (many favorited, many followers, many pictures) users.
* Active (many recent posts) users.

### Pro-celebrityness

* Many favorited and many followers criteria causes oldies effect, positive feedback, and shrink-wrap effect.
* Many pictures criteria causes oldies effect.
* Many favorited and many pictures criteria cause power user effect.

### Anti-celebrityness

* Less oldies effect: Requires recent posts.

### Summary

* Remarkably pro-celebrity user discovery.
* Better than the oldies effected user discovery methods.

## Case 9: Recently posted

### Implementation

* Profile directory [13] (Mastodon's official/embedded, default)

### Specification

* List of the users who recently posted.

### Pro-celebrity

* Power user effect: By definition.
* shrink-wrap effect: By definition.

### Anti-celebrity

* No oldies effect: By defintion.
* No positive feedback: By definition.

### Summary

* Still pro-celebrity user discovery.
* Power user effect remarkably brings pro-celebrityness.

## Case 10: Home timeline

### Implementation

* Embedded

### Specification

* Following users' posts.
* Following users' boosts.

### Pro-celebrityness

* Positive feedback: If you follow some users and boost their posts, it accelerates itself.
* Power user effect: By definition.

### Anti-celebrityness

* No oldies effect: By definition.
* No shrink-wrap effect: By definition.

### Summary

* Still pro-celebrity user discovery.
* Positive feedback remarkably brings pro-celebrityness.

## Case 11: Federated timeline

### Implementation

* Embedded

### Specification

* Local users' posts.
* Posts by the remote users who are followed by the local users.

### Pro-celebrityness

* Positive feedback: If a local user follows a remote user, the other local users may follow the remote user via their federated timeline.
* Power user effect: By definition.

### Anti-celebrityness

* No oldies effect: By definition.
* No shrink-wrap effect: By definition.

### Summary

* Still pro-celebrity user discovery.
* Mixed feature of the home and local timelines.

## Case 12: Local timeline

### Implementation

* Embedded

### Specification

* Local users' posts

### Pro-celebrityness

* Power user effect: By definition.

### Anti-celebrityness

* No oldies effect: By definition.
* No positive feedback: By definition.
* No shrink-wrap effect: By definition.

### Summary
* Still pro-celebrity user discovery.
* Power user effect remarkably brings pro-celebrityness.

## Case 13: Potential friendship

### Implementation

* Mastodon's official/embedded [14]

### Specification

* When an user engages (replies, favourites, and boosts) other user, the potential friendship counter is bumped.

### Pro- and anti-celebrityness

* Mixed feature of the home, fedarated, and local timelines [15].

### Summary

* Still pro-celebrity user discovery.

## Case 14: Search engines

### Implementations

* Tootsearch [16]
* Mastodon Realtime Search [17]
* Mastodon Search Portal [18]
* Mastodon Search by Google [19]

### Specification

* Search posts by keyword.

### Pro-celebrityness

* Power user effect: More posts, more hits.

### Anti-celebrityness

* No oldies effect: By definition.
* No positive feedback: By definition.
* No shrink-wrap effect: By definition.

### Summary

* Still pro-celebrity user discovery.
* Not suit for the user discovery use. Explicit keyword is mandatory.

## Case 15: Similar users in Mastodon Recommended User Search

### Implementation

* Mastodon Recommended User Search [6] by Cucmberium

### Specification

* The users who follow who you follow.

### Pro-celebrityness

* Shrink-wrap effect: By definition.

### Anti-celebrityness

* No oldies effect: By definition.
* No positive feedback: By definition.
* No power user effect: By definition.

### Summary

* Neutral for pro- and anti-celebrityness.
* Small cliques or clusters are formed.

## Case 16: User Matching for Fediverse

### Implementations

* User Matching for Fediverse [20] by Vaginaplant
* _Who to follow_ panel in Pleroma [21]
* _Who to follow_ panel in Halcyon [22]

### Specification

* The users who have the vocabulary which similar to you.
* Vocabulary is abstracted from one's screen name, bio, and posts.
* Users who have too much followers (500 by default) are omitted.

### Pro-celebrityness

* Shrink-wrap effect: By definition.

### Anti-celebrityness
* No oldies effect: By definition.
* Reverse positive feedback: Users who have too much followers are omitted.
* No power user effect: By definition.

### Summary

* Anti-celebrity user discovery.

## Case 17: Newcomers

### Implementations

* Profile directory [13] (Mastodon's official/embedded)
* Newcomers in Fediverse [23] by Vaginaplant

### Specification

* List of newcomers.

### Pro-celebrityness

* Shrink-wrap effect: By definition.

### Anti-celebrityness

* Reverse oldies effect: By definition.
* No positive feedback: By definition.
* No power user effect: By definition.

### Summary

* Anti-celebrity user discovery.

## References

[1] Hakaba Hitoyo, 脱中央集権のためのデザイン: セレブのためのインターネットを99 %の手に取り戻す,\
​https://web.archive.org/web/20180105170641/https://hakabahitoyo.wordpress.com/


[2] Hakaba Hitoyo, マストドンのユーザーレコメンデーションがブームに？,\
https://hakabahitoyo.wordpress.com/2018/03/29/mastodon-user-recommendation-2018 (Broken link)

​
[3] Hakaba Hitoyo, マストドンのユーザーレコメンデーションについて追記,\
​https://hakabahitoyo.wordpress.com/2018/04/18/misskey-halcyon-recommendation (Broken link)


[4] User Local, Inc., Mastodon Ranking,\
http://mastodon.userlocal.jp (Outdated)

​
[5] Osa, Recommended Followers,\
https://followlink.osa-p.net/recommend.html (Outdated)

​
[6] Cucmberium, Mastodonおすすめユーザー検索,\
https://nocona.cucmber.net/eryngium (Broken link)

​
[7] Takumi, マストドン (Mastodon) ユーザなら必ずフォローしたい！ アカウント一覧,\
https://takulog.info/mastodon-famous-accounts

​
[8] Gargron, Public profile endorsements (accounts picked by profile owner) #8146,\
https://github.com/tootsuite/mastodon/pull/8146

​
[9] Hakaba Hitoyo, Endorsement is some kind of worship or flattery,\
https://github.com/tootsuite/mastodon/pull/8146#issuecomment-411702072

​
[10] https://github.com/syuilo/misskey/blob/master/src/server/api/endpoints/users/recommendation.ts (Broken link)

​
[11] pixiv Inc., Pawoo,\
https://pawoo.net

​
[12] https://github.com/pixiv/mastodon/blob/pawoo/app/models/suggested_account_query.rb

​
[13] Gargron, Add profile directory to web UI #11688,\
https://github.com/tootsuite/mastodon/pull/11688

​
[14] Gargron, Re-add follow recommendations API #7918,\
https://github.com/tootsuite/mastodon/pull/7918

​
[15] Hakaba Hitoyo, ユーザーレコメンデーションがマストドン本体に導入された: フェアネスの評価は「現状維持」,/
https://hakabahitoyo.wordpress.com/2018/07/03/gargrons-official-user-recommendation (Broken link)

​
[16] Kirino Minato, Tootsearch,\
https://tootsearch.chotto.moe

​
[17] User Local, Inc., マストドンリアルタイム検索,\
http://realtime.userlocal.jp (Broken link)

​
[18] マストドン検索ポータル,\
http://mastodonsearch.jp (Outdated)

​
[19] Osa, マストドン検索 by google,\
https://mastosearch.osa-p.net

​
[20] Hakaba Hitoyo, User Matching for Fediverse,\
https://distsn.org/user-match.html (Broken link)

​
[21] lain, Pleroma,\
https://pleroma.social

​
[22] Neetshin, Niklas Poslovski, Halcyon,\
https://www.halcyon.social

​
[23] Hakaba Hitoyo, Newcomers in Fediverse,\
https://distsn.org/user-new.html (Broken link)

​
[24] Hakaba Hitoyo, User Recommendation Systems for Mastodon and their Fairness,\
https://hakabahitoyo.gitlab.io/slides/recommendations (Broken link)
​
